"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiamondFov = void 0;
var DiamondFov = (function () {
    function DiamondFov(mapWidth, mapHeight, isTransparent) {
        this.mapWidth = mapWidth;
        this.mapHeight = mapHeight;
        this.isTransparent = isTransparent;
    }
    DiamondFov.prototype.setMapDimensions = function (mapWidth, mapHeight) {
        this.mapWidth = mapWidth;
        this.mapHeight = mapHeight;
    };
    DiamondFov.prototype.getRay = function (data, relativeX, relativeY) {
        var x = data.originX + relativeX;
        var y = data.originY + relativeY;
        if (x >= this.mapWidth || x < 0 || y >= this.mapHeight || y < 0) {
            return;
        }
        return this.getOrMakeRay(data, x + (y * this.mapWidth), relativeX, relativeY);
    };
    DiamondFov.prototype.getOrMakeRay = function (data, index, xRelative, yRelative) {
        if (!data.rays[index]) {
            data.rays[index] = {
                x: data.originX + xRelative,
                y: data.originY + yRelative,
                ignore: false,
                touched: false,
                xError: 0,
                yError: 0,
                xRelative: xRelative,
                yRelative: yRelative,
                xObscurity: 0,
                yObscurity: 0,
                xInput: undefined,
                yInput: undefined,
                transparent: false,
                obscured: false
            };
            data.rays[index].transparent = this.isTransparent(data.rays[index].x, data.rays[index].y);
        }
        return data.rays[index];
    };
    DiamondFov.prototype.processRay = function (data, inputRay, newRay) {
        if (!newRay) {
            return;
        }
        if (newRay.yRelative === inputRay.yRelative) {
            newRay.xInput = inputRay;
        }
        else {
            newRay.yInput = inputRay;
        }
        if (!newRay.touched) {
            data.lastRayInPerimeter.nextRayInPerimeter = newRay;
            data.lastRayInPerimeter = newRay;
            newRay.touched = true;
        }
    };
    DiamondFov.prototype.processRayWithXInput = function (ray) {
        if (ray.xInput.xObscurity == 0 && ray.xInput.yObscurity == 0) {
            return;
        }
        if (ray.xInput.xError > 0 &&
            (ray.xObscurity == 0 || (ray.xInput.yError <= 0 && ray.xInput.yObscurity > 0))) {
            ray.xError = ray.xInput.xError - ray.xInput.yObscurity;
            ray.yError = ray.xInput.yError + ray.xInput.yObscurity;
            ray.xObscurity = ray.xInput.xObscurity;
            ray.yObscurity = ray.xInput.yObscurity;
        }
    };
    DiamondFov.prototype.processRayWithYInput = function (ray) {
        if (ray.yInput.xObscurity == 0 && ray.yInput.yObscurity == 0) {
            return;
        }
        if (ray.yInput.yError > 0 &&
            (ray.yObscurity == 0 || (ray.yInput.xError <= 0 && ray.yInput.xObscurity > 0))) {
            ray.yError = ray.yInput.yError - ray.yInput.xObscurity;
            ray.xError = ray.yInput.xError + ray.yInput.xObscurity;
            ray.xObscurity = ray.yInput.xObscurity;
            ray.yObscurity = ray.yInput.yObscurity;
        }
    };
    DiamondFov.prototype.processInputs = function (data, ray) {
        if (ray.xInput) {
            this.processRayWithXInput(ray);
        }
        if (ray.yInput) {
            this.processRayWithYInput(ray);
        }
        if (!ray.xInput) {
            if (ray.yInput.obscured) {
                ray.ignore = true;
                return;
            }
        }
        else if (!ray.yInput) {
            if (ray.xInput.obscured) {
                ray.ignore = true;
                return;
            }
        }
        else if (ray.xInput.obscured && ray.yInput.obscured) {
            ray.ignore = true;
            if (!ray.transparent && !ray.xInput.transparent && !ray.yInput.transparent) {
                data.setVisible(ray.x, ray.y);
            }
            return;
        }
        ray.obscured = (ray.xError > 0 && ray.xError <= ray.xObscurity) ||
            (ray.yError > 0 && ray.yError <= ray.yObscurity);
        if (!ray.transparent) {
            if (!ray.obscured) {
                data.setVisible(ray.x, ray.y);
            }
            else if (ray.xInput && !ray.xInput.transparent || ray.yInput && !ray.yInput.transparent) {
                data.setVisible(ray.x, ray.y);
            }
            else if (ray.xObscurity !== ray.yObscurity
                && ((ray.xInput && ray.xInput.transparent && !ray.xInput.obscured)
                    || (ray.yInput && ray.yInput.transparent && !ray.yInput.obscured))) {
                data.setVisible(ray.x, ray.y);
            }
            ray.obscured = true;
            ray.xError = ray.xObscurity = Math.abs(ray.xRelative);
            ray.yError = ray.yObscurity = Math.abs(ray.yRelative);
        }
    };
    DiamondFov.prototype.expandPerimeterFrom = function (data, ray) {
        if (ray.ignore) {
            return;
        }
        if (ray.xRelative >= 0) {
            this.processRay(data, ray, this.getRay(data, ray.xRelative + 1, ray.yRelative));
        }
        if (ray.xRelative <= 0) {
            this.processRay(data, ray, this.getRay(data, ray.xRelative - 1, ray.yRelative));
        }
        if (ray.yRelative >= 0) {
            this.processRay(data, ray, this.getRay(data, ray.xRelative, ray.yRelative + 1));
        }
        if (ray.yRelative <= 0) {
            this.processRay(data, ray, this.getRay(data, ray.xRelative, ray.yRelative - 1));
        }
    };
    DiamondFov.prototype.compute = function (originX, originY, radius, setVisible) {
        var data = {
            originX: originX,
            originY: originY,
            radius: radius > 0 ? radius * radius : 0,
            rays: [],
            setVisible: setVisible
        };
        if (originX < 0 || originX >= this.mapWidth || originY < 0 || originY >= this.mapHeight) {
            return;
        }
        var currentRay = data.lastRayInPerimeter = this.getRay(data, 0, 0);
        currentRay.touched = true;
        data.setVisible(currentRay.x, currentRay.y);
        this.expandPerimeterFrom(data, currentRay);
        while (currentRay = currentRay.nextRayInPerimeter) {
            this.processInputs(data, currentRay);
            this.expandPerimeterFrom(data, currentRay);
            if (currentRay.ignore) {
                continue;
            }
            if (currentRay.obscured) {
                continue;
            }
            data.setVisible(currentRay.x, currentRay.y);
        }
    };
    return DiamondFov;
}());
exports.DiamondFov = DiamondFov;
