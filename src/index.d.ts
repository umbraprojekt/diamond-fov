export declare type IsTransparentFunc = (x: number, y: number) => boolean;
export declare type SetVisibleFunc = (x: number, y: number) => void;
export declare class DiamondFov {
    private mapWidth;
    private mapHeight;
    private readonly isTransparent;
    constructor(mapWidth: number, mapHeight: number, isTransparent: IsTransparentFunc);
    setMapDimensions(mapWidth: number, mapHeight: number): void;
    private getRay;
    private getOrMakeRay;
    private processRay;
    private processRayWithXInput;
    private processRayWithYInput;
    private processInputs;
    private expandPerimeterFrom;
    compute(originX: number, originY: number, radius: number, setVisible: SetVisibleFunc): void;
}
