export type IsTransparentFunc = (x: number, y: number) => boolean;
export type SetVisibleFunc = (x: number, y: number) => void;

interface Ray {
	x: number;
	y: number;
	xRelative: number;
	yRelative: number;
	xObscurity: number;
	yObscurity: number;
	xError: number;
	yError: number;
	xInput: Ray;
	yInput: Ray;
	nextRayInPerimeter?: Ray;
	touched: boolean;
	ignore: boolean;
	transparent: boolean;
	obscured: boolean;
}

interface FovData {
	originX: number;
	originY: number;
	radius: number;
	rays: Ray[];
	lastRayInPerimeter?: Ray;
	setVisible: SetVisibleFunc;
}

export class DiamondFov {

	public constructor(
		private mapWidth: number,
		private mapHeight: number,
		private readonly isTransparent: IsTransparentFunc
	) { }

	public setMapDimensions(mapWidth: number, mapHeight: number): void {
		this.mapWidth = mapWidth;
		this.mapHeight = mapHeight;
	}

	private getRay(data: FovData, relativeX: number, relativeY: number): Ray | undefined {
		const x = data.originX + relativeX;
		const y = data.originY + relativeY;

		if (x >= this.mapWidth || x < 0 || y >= this.mapHeight || y < 0) {
			return;
		}

		return this.getOrMakeRay(data, x + (y * this.mapWidth), relativeX, relativeY);
	}

	private getOrMakeRay(data: FovData, index: number, xRelative: number, yRelative: number): Ray {
		if (!data.rays[index]) {
			data.rays[index] = {
				x: data.originX + xRelative,
				y: data.originY + yRelative,
				ignore: false,
				touched: false,
				xError: 0,
				yError: 0,
				xRelative,
				yRelative,
				xObscurity: 0,
				yObscurity: 0,
				xInput: undefined as any,
				yInput: undefined as any,
				transparent: false,
				obscured: false
			};
			data.rays[index].transparent = this.isTransparent(data.rays[index].x, data.rays[index].y);
		}
		return data.rays[index];
	}

	/**
	 * Set newly spawned ray's X or Y input and add it to the perimeter
	 * @param data FOV data
	 * @param inputRay the ray that spawned the new ray
	 * @param newRay the newly spawned ray
	 */
	private processRay(data: FovData, inputRay: Ray, newRay?: Ray): void {
		if (!newRay) {
			return;
		}
		if (newRay.yRelative === inputRay.yRelative) { // move in X direction
			newRay.xInput = inputRay;
		} else { // move in Y direction
			newRay.yInput = inputRay;
		}
		if (!newRay.touched) { // If the tile is newly created, add to the perimeter
			data.lastRayInPerimeter!.nextRayInPerimeter = newRay as Ray;
			data.lastRayInPerimeter = newRay;
			newRay.touched = true;
		}
	}

	/**
	 * Compute the error and obscurity vectors of a ray in X direction
	 * @param ray a ray with an X input
	 */
	private processRayWithXInput(ray: Ray): void {
		if (ray.xInput.xObscurity == 0 && ray.xInput.yObscurity == 0) { // completely unobstructed
			return;
		}
		if (ray.xInput.xError > 0 &&
			(ray.xObscurity == 0 || (ray.xInput.yError <= 0 && ray.xInput.yObscurity > 0))) {
			ray.xError = ray.xInput.xError - ray.xInput.yObscurity;
			ray.yError = ray.xInput.yError + ray.xInput.yObscurity;
			ray.xObscurity = ray.xInput.xObscurity;
			ray.yObscurity = ray.xInput.yObscurity;
		}
	}

	/**
	 * Compute the error and obscurity vectors of ray in Y direction
	 * @param ray a ray with a Y input
	 */
	private processRayWithYInput(ray: Ray): void {
		if (ray.yInput.xObscurity == 0 && ray.yInput.yObscurity == 0) { // completely unobstructed
			return;
		}
		if (ray.yInput.yError > 0 &&
			(ray.yObscurity == 0 || (ray.yInput.xError <= 0 && ray.yInput.xObscurity > 0))) {
			ray.yError = ray.yInput.yError - ray.yInput.xObscurity;
			ray.xError = ray.yInput.xError + ray.yInput.xObscurity;
			ray.xObscurity = ray.yInput.xObscurity;
			ray.yObscurity = ray.yInput.yObscurity;
		}
	}

	/**
	 * Compute the error and obscurity vectors of a ray.
	 * Only called on spawned rays (not origin), so there's always at least one input.
	 * @param newRay newly spawned ray
	 * @param xInput input ray that spawned the new one
	 */
	private processInputs(data: FovData, ray: Ray): void {
		if (ray.xInput) {
			this.processRayWithXInput(ray);
		}
		if (ray.yInput) {
			this.processRayWithYInput(ray);
		}

		// ignore if:
		if (!ray.xInput) {
			if (ray.yInput.obscured) { // vertical ray and input is obscured
				ray.ignore = true;
				return;
			}
		} else if (!ray.yInput) {
			if (ray.xInput.obscured) { // horizontal ray and input is obcsured
				ray.ignore = true;
				return;
			}
		} else if (ray.xInput.obscured && ray.yInput.obscured) { // both inputs are obscured
			ray.ignore = true;

			// corner case (literally): check if it's a corner. If so, light it and abandon it.
			if (!ray.transparent && !ray.xInput.transparent && !ray.yInput.transparent) {
				data.setVisible(ray.x, ray.y);
			}
			return;
		}

		// check if the ray is obscured
		ray.obscured = (ray.xError > 0 && ray.xError <= ray.xObscurity) ||
			(ray.yError > 0 && ray.yError <= ray.yObscurity);

		// wall treatment
		if (!ray.transparent) {
			// light the wall in one of the following cases:
			if (
				!ray.obscured // it's unobscured
				|| ray.xInput && !ray.xInput.transparent || ray.yInput && !ray.yInput.transparent // its input is also a wall
				|| (ray.xObscurity !== ray.yObscurity // its input is an unobstructed transparent tile AND it's not behind a wall diagonally
					&& ((ray.xInput && ray.xInput.transparent && !ray.xInput.obscured)
						|| (ray.yInput && ray.yInput.transparent && !ray.yInput.obscured)))
			) {
				data.setVisible(ray.x, ray.y);
			}

			// walls always become obscured after treatment
			ray.obscured = true;

			// set new error and obscurity data
			ray.xError = ray.xObscurity = Math.abs(ray.xRelative);
			ray.yError = ray.yObscurity = Math.abs(ray.yRelative);
		}
	}

	private expandPerimeterFrom(data: FovData, ray: Ray): void {
		if (ray.ignore) {
			return;
		}
		if (ray.xRelative >= 0) {
			this.processRay(data, ray, this.getRay(data, ray.xRelative + 1, ray.yRelative));
		}
		if (ray.xRelative <= 0) {
			this.processRay(data, ray, this.getRay(data, ray.xRelative - 1, ray.yRelative));
		}
		if (ray.yRelative >= 0) {
			this.processRay(data, ray, this.getRay(data, ray.xRelative, ray.yRelative + 1));
		}
		if (ray.yRelative <= 0) {
			this.processRay(data, ray, this.getRay(data, ray.xRelative, ray.yRelative - 1));
		}
	}

	public compute(originX: number, originY: number, radius: number, setVisible: SetVisibleFunc): void {
		const data: FovData = {
			originX,
			originY,
			radius: radius > 0 ? radius * radius : 0,
			rays: [],
			setVisible
		};

		if (originX < 0 || originX >= this.mapWidth || originY < 0 || originY >= this.mapHeight) {
			return;
		}

		// Add the origin ray tile to start the process.
		let currentRay: Ray = data.lastRayInPerimeter = this.getRay(data, 0, 0) as Ray;
		currentRay.touched = true;
		data.setVisible(currentRay.x, currentRay.y);
		this.expandPerimeterFrom(data, currentRay);

		// Iterate over the diamond perimeter.
		while (currentRay = currentRay.nextRayInPerimeter!) {
			this.processInputs(data, currentRay);
			this.expandPerimeterFrom(data, currentRay);

			// If not visible (ignored), go to next ray
			if (currentRay.ignore) {
				continue;
			}

			// If not visible (obscured) and transparent, go to next ray
			if (currentRay.obscured) {
				continue;
			}

			// If not ignored and either not obscured or opaque, set visible
			data.setVisible(currentRay.x, currentRay.y);
		}
	}
}
